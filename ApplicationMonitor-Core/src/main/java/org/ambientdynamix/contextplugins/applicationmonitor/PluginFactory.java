package org.ambientdynamix.contextplugins.applicationmonitor;

import org.ambientdynamix.api.contextplugin.ContextPluginRuntimeFactory;

public class PluginFactory extends ContextPluginRuntimeFactory {

	public PluginFactory() {
		super(ApplicationMonitorPluginRuntime.class);
	}

}

