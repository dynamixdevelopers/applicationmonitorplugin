package org.ambientdynamix.contextplugins.applicationmonitor;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import org.ambientdynamix.api.application.ErrorCodes;
import org.ambientdynamix.api.contextplugin.ContextPluginRuntime;
import org.ambientdynamix.api.contextplugin.ContextPluginSettings;
import org.ambientdynamix.api.contextplugin.PowerScheme;

import java.util.List;
import java.util.UUID;
import java.util.Vector;

public class ApplicationMonitorPluginRuntime extends ContextPluginRuntime {
	
	private static final int VALID_CONTEXT_DURATION = 60000;
	// Static logging TAG
	private final String TAG = this.getClass().getSimpleName();
	// Our secure context
	private Context context;

	@Override
	public void handleConfiguredContextRequest(UUID requestId, String contextType,
			Bundle arg2) {
		// Warn that we don't handle configured requests
				Log.w(TAG, "handleConfiguredContextRequest called, but we don't support configuration!");
				// Drop the config and default to handleContextRequest
				handleContextRequest(requestId, contextType);
		
	}

	@Override
	public void handleContextRequest(UUID requestId, String contextType) {
		Log.d(TAG,"got context request with type " + contextType);
		// Check for proper context type
				if (contextType.equalsIgnoreCase(InstalledApplicationInfo.CONTEXT_TYPE)) {
					// Manually access the Application list
					List<String> applicationNames = new Vector<String>();
					if(null == context)
						Log.e(TAG,"context is null, init has failed");
					List<ApplicationInfo> applications = getInstalledApplication(context);
					if(applications!=null){
						for (ApplicationInfo applicationInfo : applications) {
							Log.d(TAG,applicationInfo.toString());
							applicationNames.add(applicationInfo.toString());
						}
					}else{
						Log.e(TAG,"unable to access installed applications list");
					}
					// Send the context event
					Log.i(TAG,"sending context event with list of applications");
					
					sendContextEvent(requestId,new InstalledApplicationInfo(applicationNames), VALID_CONTEXT_DURATION);
					
				} else if(contextType.equalsIgnoreCase(RunningProcessesInfoList.CONTEXT_TYPE)){
					ActivityManager actvityManager = (ActivityManager)context.getSystemService( Context.ACTIVITY_SERVICE );
					if(null==actvityManager){
						Log.e(TAG,"can't get activity manager");
					}
					List<RunningAppProcessInfo> procInfos = actvityManager.getRunningAppProcesses();						
					sendContextEvent(requestId, new RunningProcessesInfoList(procInfos), VALID_CONTEXT_DURATION);
				}
				
				else  {
					sendContextRequestError(requestId, "NO_CONTEXT_SUPPORT for " + contextType, ErrorCodes.CONTEXT_SUPPORT_NOT_FOUND);
				}
		
	}

	@Override
	public void destroy() throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void init(PowerScheme powerScheme, ContextPluginSettings arg1)
			throws Exception {
		// Set the power scheme
		this.setPowerScheme(powerScheme);
		// Store our secure context
		this.context = this.getSecuredContext();
	}

	@Override
	public void setPowerScheme(PowerScheme arg0) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void start() throws Exception {
		
		Log.d(TAG, "Started!");			
	}

	@Override
	public void stop() throws Exception {
		// TODO Auto-generated method stub
		
	}
	
	public List<ApplicationInfo> getInstalledApplication(Context c) {
		
	    PackageManager p = c.getPackageManager();
	    if(null != p)
	    	    return p.getInstalledApplications(PackageManager.GET_META_DATA);
	    Log.e(TAG, "cannot get packageManager" );
	    return new Vector<ApplicationInfo>();
	}


}
