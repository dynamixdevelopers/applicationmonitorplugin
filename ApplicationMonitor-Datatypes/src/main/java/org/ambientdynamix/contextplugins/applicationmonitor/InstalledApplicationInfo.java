/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.contextplugins.applicationmonitor;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import android.os.Parcel;
import android.os.Parcelable;

class InstalledApplicationInfo implements IInstalledApplicationInfo {
	/**
	 * Required CREATOR field that generates instances of this Parcelable class from a Parcel.
	 * 
	 * @see http://developer.android.com/reference/android/os/Parcelable.Creator.html
	 */
	public static Parcelable.Creator<InstalledApplicationInfo> CREATOR = new Parcelable.Creator<InstalledApplicationInfo>() {
		/**
		 * Create a new instance of the Parcelable class, instantiating it from the given Parcel whose data had
		 * previously been written by Parcelable.writeToParcel().
		 */
		public InstalledApplicationInfo createFromParcel(Parcel in) {
			return new InstalledApplicationInfo(in);
		}

		/**
		 * Create a new array of the Parcelable class.
		 */
		public InstalledApplicationInfo[] newArray(int size) {
			return new InstalledApplicationInfo[size];
		}
	};
	// Public static variable for our supported context type
	public static String CONTEXT_TYPE = "org.ambientdynamix.contextplugins.applicationmonitor.installedapplications";
	// Private data
	private List<String> applications;
	/**
	 * Returns the type of the context information represented by the IContextInfo. This string must match one of the
	 * supported context information type strings described by the source ContextPlugin.
	 */
	@Override
	public String getContextType() {
		return CONTEXT_TYPE;
	}

	/**
	 * Returns the fully qualified class-name of the class implementing the IContextInfo interface. This allows Dynamix
	 * applications to dynamically cast IContextInfo objects to their original type using reflection. A Java
	 * "instanceof" compare can also be used for this purpose.
	 */
	@Override
	public String getImplementingClassname() {
		return this.getClass().getName();
	}

	/**
	 * Returns a Set of supported string-based context representation format types or null if no representation formats
	 * are supported. Examples formats could include MIME, Dublin Core, RDF, etc. See the plug-in documentation for
	 * supported representation types.
	 */
	@Override
	public Set<String> getStringRepresentationFormats() {
		Set<String> formats = new HashSet<String>();
		formats.add("text/plain");
		return formats;
	}

	/**
	 * Returns a string-based representation of the IContextInfo for the specified format string (e.g.
	 * "application/json") or null if the requested format is not supported.
	 */
	@Override
	public String getStringRepresentation(String format) {
		if (format.equalsIgnoreCase("text/plain")){
			String applicationsCsv = "";
			for (String application : applications) {
				applicationsCsv = applicationsCsv + "," +application;
			}
			return applicationsCsv;
		}
			
		else
			// Format not supported, so return an empty string
			return "";
	}

	/**
	 * Create a InstalledApplicationInfo
	 * 
	 * @param applications list
	 *            
	 */
	public InstalledApplicationInfo(List<String> applications) {
		this.applications = applications; 
	}

	

	@Override
	public String toString() {
		return this.getClass().getSimpleName();
	};

	/**
	 * Used by Parcelable when sending (serializing) data over IPC.
	 */
	public void writeToParcel(Parcel out, int flags) {
		out.writeInt(applications.size());
		for (String application : applications) {
			out.writeString(application);
		}
		
	}

	/**
	 * Used by the Parcelable.Creator when reconstructing (deserializing) data sent over IPC.
	 */
	private InstalledApplicationInfo(final Parcel in) {		
		int numApps = in.readInt();
		if(applications==null)
			applications = new Vector<String>();
		
		for(int i =0;i<numApps;i++){
			applications.add(in.readString());
		}		
	}

	/**
	 * Default implementation that returns 0.
	 * 
	 * @return 0
	 */
	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public List<String> getInstalledApplications() {
		return applications;
	}
}