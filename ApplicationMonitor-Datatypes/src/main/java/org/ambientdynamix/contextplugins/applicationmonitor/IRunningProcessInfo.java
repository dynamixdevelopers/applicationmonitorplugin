package org.ambientdynamix.contextplugins.applicationmonitor;

import org.ambientdynamix.api.application.IContextInfo;

import android.content.ComponentName;

public interface IRunningProcessInfo extends IContextInfo{

	public int getImportance();

	public int getImportanceReasonCode();

	public int getImportanceReasonPid();

	public ComponentName getImportanceReasonComponent();

	public int getPid();

	public int getUid();

	public int getLru();

	public int getLastTrimLevel();

	public String[] getPackageList();

	public String getProecssName();


}