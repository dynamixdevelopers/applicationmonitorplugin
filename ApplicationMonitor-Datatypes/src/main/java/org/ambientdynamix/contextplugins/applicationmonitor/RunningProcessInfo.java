package org.ambientdynamix.contextplugins.applicationmonitor;

import android.app.ActivityManager.RunningAppProcessInfo;
import android.content.ComponentName;
import android.os.Parcel;
import android.os.Parcelable;

public class RunningProcessInfo implements Parcelable {

	public static final int IMPORTANCE_FOREGROUND = RunningAppProcessInfo.IMPORTANCE_FOREGROUND;
	public static final int IMPORTANCE_BACKGROUND = RunningAppProcessInfo.IMPORTANCE_BACKGROUND;
	public static final int IMPORTANCE_EMPTY = RunningAppProcessInfo.IMPORTANCE_EMPTY;
	public static final int IMPORTANCE_PERCEPTIBLE = RunningAppProcessInfo.IMPORTANCE_PERCEPTIBLE;
	public static final int IMPORTANCE_SERVICE = RunningAppProcessInfo.IMPORTANCE_SERVICE;
	public static final int IMPORTANCE_VISIBLE = RunningAppProcessInfo.IMPORTANCE_VISIBLE;
	public static final int REASON_PROVIDER_IN_USE = RunningAppProcessInfo.REASON_PROVIDER_IN_USE;
	public static final int REASON_SERVICE_IN_USE = RunningAppProcessInfo.REASON_SERVICE_IN_USE;
	public static final int REASON_UNKNOWN = RunningAppProcessInfo.REASON_UNKNOWN;

	private int importance;
	private int importanceReasonCode;
	private int importanceReasonPid;
	private ComponentName importanceReasonComponent;
	private int pid;
	private int uid;
	private int lru;
	private int lastTrimLevel;
	private String[] packageList;
	private String proecssName;

	public static Parcelable.Creator<RunningProcessInfo> CREATOR = new Parcelable.Creator<RunningProcessInfo>() {
		/**
		 * Create a new instance of the Parcelable class, instantiating it from
		 * the given Parcel whose data had previously been written by
		 * Parcelable.writeToParcel().
		 */
		public RunningProcessInfo createFromParcel(Parcel in) {
			return new RunningProcessInfo(in);
		}

		/**
		 * Create a new array of the Parcelable class.
		 */
		public RunningProcessInfo[] newArray(int size) {
			return new RunningProcessInfo[size];
		}
	};

	/**
	 * Returns the type of the context information represented by the
	 * IContextInfo. This string must match one of the supported context
	 * information type strings described by the source ContextPlugin.
	 */

	public RunningProcessInfo(RunningAppProcessInfo info) {
		this.importance = info.importance;
		this.importanceReasonCode = info.importanceReasonCode;
		this.importanceReasonComponent = info.importanceReasonComponent;
		this.importanceReasonPid = info.importanceReasonPid;
		this.lastTrimLevel = info.lastTrimLevel;
		this.lru = info.lru;
		this.packageList = info.pkgList;
		this.pid = info.pid;
		this.proecssName = info.processName;
		this.uid = info.uid;

	}

	public int getImportance() {
		return importance;
	}

	public int getImportanceReasonCode() {
		return importanceReasonCode;
	}

	public int getImportanceReasonPid() {
		return importanceReasonPid;
	}

	public ComponentName getImportanceReasonComponent() {
		return importanceReasonComponent;
	}

	public int getPid() {
		return pid;
	}

	public int getUid() {
		return uid;
	}

	public int getLru() {
		return lru;
	}

	public int getLastTrimLevel() {
		return lastTrimLevel;
	}

	public String[] getPackageList() {
		return packageList;
	}

	public String getProecssName() {
		return proecssName;
	}

	/**
	 * Default implementation that returns 0.
	 * 
	 * @return 0
	 */
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	public RunningProcessInfo(Parcel in) {
		importance = in.readInt();
		importanceReasonCode = in.readInt();
		importanceReasonPid = in.readInt();
		importanceReasonComponent = in.readParcelable(ComponentName.class
				.getClassLoader());
		pid = in.readInt();
		uid = in.readInt();
		lru = in.readInt();
		lastTrimLevel = in.readInt();
		packageList = in.createStringArray();
		proecssName = in.readString();
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(importance);
		dest.writeInt(importanceReasonCode);
		dest.writeInt(importanceReasonPid);
		dest.writeParcelable(importanceReasonComponent, 0);
		dest.writeInt(pid);
		dest.writeInt(uid);
		dest.writeInt(lru);
		dest.writeInt(lastTrimLevel);
		dest.writeStringArray(packageList);
		dest.writeString(proecssName);
	}
}
