package org.ambientdynamix.contextplugins.applicationmonitor;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import android.app.ActivityManager.RunningAppProcessInfo;
import android.os.Parcel;
import android.os.Parcelable;

public class RunningProcessesInfoList implements IRunningProcessesInfoList {
	
	

	/**
	 * Required CREATOR field that generates instances of this Parcelable class from a Parcel.
	 * 
	 * @see http://developer.android.com/reference/android/os/Parcelable.Creator.html
	 */
	public static Parcelable.Creator<RunningProcessesInfoList> CREATOR = new Parcelable.Creator<RunningProcessesInfoList>() {
		/**
		 * Create a new instance of the Parcelable class, instantiating it from the given Parcel whose data had
		 * previously been written by Parcelable.writeToParcel().
		 */
		public RunningProcessesInfoList createFromParcel(Parcel in) {
			return new RunningProcessesInfoList(in);
		}

		/**
		 * Create a new array of the Parcelable class.
		 */
		public RunningProcessesInfoList[] newArray(int size) {
			return new RunningProcessesInfoList[size];
		}
	};
	// Public static variable for our supported context type
	public static String CONTEXT_TYPE = "org.ambientdynamix.contextplugins.applicationmonitor.runningprocesseslist";
	// Private data
	private List<RunningProcessInfo> processes;
	/**
	 * Returns the type of the context information represented by the IContextInfo. This string must match one of the
	 * supported context information type strings described by the source ContextPlugin.
	 */
	@Override
	public String getContextType() {
		return CONTEXT_TYPE;
	}

	/**
	 * Returns the fully qualified class-name of the class implementing the IContextInfo interface. This allows Dynamix
	 * applications to dynamically cast IContextInfo objects to their original type using reflection. A Java
	 * "instanceof" compare can also be used for this purpose.
	 */
	@Override
	public String getImplementingClassname() {
		return this.getClass().getName();
	}

	/**
	 * Returns a Set of supported string-based context representation format types or null if no representation formats
	 * are supported. Examples formats could include MIME, Dublin Core, RDF, etc. See the plug-in documentation for
	 * supported representation types.
	 */
	@Override
	public Set<String> getStringRepresentationFormats() {
		Set<String> formats = new HashSet<String>();
		formats.add("text/plain");
		return formats;
	}

	/**
	 * Returns a string-based representation of the IContextInfo for the specified format string (e.g.
	 * "application/json") or null if the requested format is not supported.
	 */
	@Override
	public String getStringRepresentation(String format) {
		if (format.equalsIgnoreCase("text/plain")){
			String processesCsv = "";
			for (RunningProcessInfo process : processes) {
				processesCsv = processesCsv + "," + process.getProecssName();
			}
			return processesCsv;
		}
			
		else
			// Format not supported, so return an empty string
			return "";
	}

	/**
	 * Create a RunningProcessesInfo
	 * 
	 * @param processes list
	 *            
	 */
//	public RunningProcessesInfo(List<String> processes) {
//		this.processes = processes;
//	}

	

	@Override
	public String toString() {
		return this.getClass().getSimpleName();
	};

	/**
	 * Used by Parcelable when sending (serializing) data over IPC.
	 */
	public void writeToParcel(Parcel out, int flags) {
		if(processes != null)
			out.writeList(processes);
		
	}

	/**
	 * Used by the Parcelable.Creator when reconstructing (deserializing) data sent over IPC.
	 */
	private RunningProcessesInfoList(final Parcel in) {
		if(processes==null)
			processes = new Vector<RunningProcessInfo>();	
		in.readList(processes, RunningProcessInfo.class.getClassLoader());		
	}

	public RunningProcessesInfoList(List<RunningAppProcessInfo> procInfos) {
		this.processes = new Vector<RunningProcessInfo>();
		for (RunningAppProcessInfo runningAppProcessInfo : procInfos) {
			processes.add(new RunningProcessInfo(runningAppProcessInfo));
		}
		
	}

	/**
	 * Default implementation that returns 0.
	 * 
	 * @return 0
	 */
	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public List<RunningProcessInfo> getRunningProcesses() {		
		return processes;
	}
	

}
