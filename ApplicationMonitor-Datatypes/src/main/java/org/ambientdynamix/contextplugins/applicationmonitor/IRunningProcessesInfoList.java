package org.ambientdynamix.contextplugins.applicationmonitor;

import java.util.List;

import org.ambientdynamix.api.application.IContextInfo;

public interface IRunningProcessesInfoList extends IContextInfo{
	
	public List<RunningProcessInfo> getRunningProcesses();

}
