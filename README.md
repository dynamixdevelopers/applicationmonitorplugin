# The Application Monitor Plugin
This plugin provides information about running processes and installed applications. See the [Dynamix Framework documentation](http://ambientdynamix.org/documentation/) to learn how to use the Dynamix framework and add support for this plugin.

The supported context types are:

`org.ambientdynamix.contextplugins.applicationmonitor.installedapplications`
Gives you a list of installed applications and related infos

`org.ambientdynamix.contextplugins.applicationmonitor.runningprocesseslist`
gives you a lits of currently running processes and related infos.